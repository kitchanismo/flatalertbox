﻿namespace FlatAlertBoxDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShow = new System.Windows.Forms.Button();
            this.txtMessaage = new System.Windows.Forms.TextBox();
            this.rbSuccess = new System.Windows.Forms.RadioButton();
            this.rbInfo = new System.Windows.Forms.RadioButton();
            this.rbError = new System.Windows.Forms.RadioButton();
            this.Duration = new System.Windows.Forms.NumericUpDown();
            this.flatAlertBox1 = new FlatAlert.FlatAlertBox();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.Duration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShow
            // 
            this.btnShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShow.Location = new System.Drawing.Point(57, 175);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(304, 59);
            this.btnShow.TabIndex = 0;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // txtMessaage
            // 
            this.txtMessaage.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessaage.Location = new System.Drawing.Point(57, 80);
            this.txtMessaage.Name = "txtMessaage";
            this.txtMessaage.Size = new System.Drawing.Size(208, 27);
            this.txtMessaage.TabIndex = 4;
            this.txtMessaage.Text = "Hello World!";
            this.txtMessaage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rbSuccess
            // 
            this.rbSuccess.AutoSize = true;
            this.rbSuccess.Checked = true;
            this.rbSuccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbSuccess.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSuccess.Location = new System.Drawing.Point(57, 125);
            this.rbSuccess.Name = "rbSuccess";
            this.rbSuccess.Size = new System.Drawing.Size(87, 25);
            this.rbSuccess.TabIndex = 5;
            this.rbSuccess.TabStop = true;
            this.rbSuccess.Text = "Success";
            this.rbSuccess.UseVisualStyleBackColor = true;
            // 
            // rbInfo
            // 
            this.rbInfo.AutoSize = true;
            this.rbInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbInfo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbInfo.Location = new System.Drawing.Point(187, 125);
            this.rbInfo.Name = "rbInfo";
            this.rbInfo.Size = new System.Drawing.Size(57, 25);
            this.rbInfo.TabIndex = 6;
            this.rbInfo.Text = "Info";
            this.rbInfo.UseVisualStyleBackColor = true;
            // 
            // rbError
            // 
            this.rbError.AutoSize = true;
            this.rbError.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbError.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbError.Location = new System.Drawing.Point(309, 125);
            this.rbError.Name = "rbError";
            this.rbError.Size = new System.Drawing.Size(61, 25);
            this.rbError.TabIndex = 7;
            this.rbError.Text = "Error";
            this.rbError.UseVisualStyleBackColor = true;
            // 
            // Duration
            // 
            this.Duration.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Duration.Location = new System.Drawing.Point(280, 81);
            this.Duration.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.Duration.Name = "Duration";
            this.Duration.Size = new System.Drawing.Size(81, 27);
            this.Duration.TabIndex = 8;
            this.Duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Duration.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // flatAlertBox1
            // 
            this.flatAlertBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(73)))));
            this.flatAlertBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatAlertBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatAlertBox1.kind = FlatAlert.FlatAlertBox._Kind.Success;
            this.flatAlertBox1.Location = new System.Drawing.Point(12, 12);
            this.flatAlertBox1.Name = "flatAlertBox1";
            this.flatAlertBox1.Size = new System.Drawing.Size(424, 42);
            this.flatAlertBox1.TabIndex = 3;
            this.flatAlertBox1.Text = "flatAlertBox1";
            this.flatAlertBox1.Visible = false;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(448, 264);
            this.Controls.Add(this.Duration);
            this.Controls.Add(this.rbError);
            this.Controls.Add(this.rbInfo);
            this.Controls.Add(this.rbSuccess);
            this.Controls.Add(this.txtMessaage);
            this.Controls.Add(this.flatAlertBox1);
            this.Controls.Add(this.btnShow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FlatAlert";
            ((System.ComponentModel.ISupportInitialize)(this.Duration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnShow;
        private FlatAlert.FlatAlertBox flatAlertBox1;
        private System.Windows.Forms.TextBox txtMessaage;
        private System.Windows.Forms.RadioButton rbSuccess;
        private System.Windows.Forms.RadioButton rbInfo;
        private System.Windows.Forms.RadioButton rbError;
        private System.Windows.Forms.NumericUpDown Duration;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
    }
}

