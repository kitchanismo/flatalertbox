﻿using System;
using System.Windows.Forms;
using static FlatAlert.FlatAlertBox;

namespace FlatAlertBoxDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            flatAlertBox1.Dock = DockStyle.Top;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            //[Message],[Kind],[Duration]
            flatAlertBox1.Show(txtMessaage.Text, Kind ,(int)Duration.Value);
        }
        
        private _Kind Kind {
        get {
                if (rbSuccess.Checked)
                    return _Kind.Success;
                if (rbInfo.Checked)
                    return _Kind.Info;
                if (rbError.Checked)
                    return _Kind.Error;
                throw new Exception("Not implemented");
            }
        }

    }
}
